#include <ros/ros.h>
#include <std_srvs/Trigger.h>

using namespace std;

string call_service(const string &srv_name)
{
  std_srvs::Trigger::Request req; 
  std_srvs::Trigger::Response res; 

  ROS_WARN("%s", srv_name.c_str());

  if (ros::service::call(srv_name, req, res))
  {
    return res.message;
  }
  else
  {
    ROS_ERROR("Unable to call service \"%s\"", srv_name.c_str());
    return "Unable to call service";
  }
}
enum Admin
{
	INSPECT=1,
	DRIVE_TO_OBJ_1=2,
	ROTATE_PI_1=3,
	TAKE_1=4,
	MOVE_AWAY=5,
	ROTATE_PI_2=6,
	DRIVE_TO_OBJ_2=7,
	ROTATE_PI_3=8,
	TAKE_2=9,
	COME_BACK=10,
	COMMUNICATION=11
};
int main(int argc, char* argv[])
{
  // Инициализация ноды
  ros::init(argc, argv, "state_machine");
  ros::NodeHandle nh;

  enum Admin state=INSPECT;
  string s;
  ros::service::waitForService("/skills/inspect");
  // Главный цикл, пока нода функционирует
  while (ros::ok())
  {
    if(state==INSPECT)
    {
      s=call_service("/skills/inspect");
      state=DRIVE_TO_OBJ_1;
    }
    else if(state==DRIVE_TO_OBJ_1)
    {
      s=call_service("/skills/drive_to_obj");
      if(s=="DONE")state=ROTATE_PI_1;
      else if(s=="FAILED")state=INSPECT;
    }
    else if(state==ROTATE_PI_1)
    {
      s=call_service("/skills/rotate_pi");
      state=TAKE_1;
    }
    else if(state==TAKE_1)
    {
      s=call_service("/skills/take");
      if(s=="DONE")state=COME_BACK;
      else if(s=="FAILED")state=MOVE_AWAY;
    }
    else if(state==MOVE_AWAY)
    {
      s=call_service("/skills/move_away");
      state=ROTATE_PI_2;
    }
    else if(state==ROTATE_PI_2)
    {
      s=call_service("/skills/rotate_pi");
      state=DRIVE_TO_OBJ_2;
    }
    else if(state==DRIVE_TO_OBJ_2)
    {
      s=call_service("/skills/drive_to_obj");
      if(s=="DONE")state=ROTATE_PI_3;
      if(s=="FAILED")state=INSPECT;
    }
    else if(state==ROTATE_PI_3)
    {
      s=call_service("/skills/rotate_pi");
      state=TAKE_2;
    }
    else if(state==TAKE_2)
    {
      s=call_service("/skills/take");
      if(s=="DONE")state=COME_BACK;
      if(s=="FAILED")state=INSPECT;
    }
    else if(state==COME_BACK)
    {
      s=call_service("/skills/come_back");
      state=COMMUNICATION;
    }
    else if(state==COMMUNICATION)
    {
      //ROS_WARN("Operator request");//связь с оператором
      s=call_service("/skills/communication");
      state=INSPECT;
    }
    else
    {
       ROS_WARN("bug");
    }
    ROS_WARN("state: %d, res: %s ", state, s.c_str());
  }
  return 0;
}

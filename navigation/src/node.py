import sys
import yaml
import rospy
from std_srvs.srv import Trigger, TriggerResponse
from trash_detector.srv import ObjImgPos
import actionlib
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal


CLIENT = None
IMAGE = None
POINTS = []
INDEX = 0
INTERVAL = 0.5


def check_image():
  resp = IMAGE()

  # abs(x) > 1 - No image

  return abs(resp.data) <= 1


def send_goal(point):
  global CLIENT

  goal = MoveBaseGoal()
  goal.target_pose.header.frame_id = "map"
  goal.target_pose.header.stamp = rospy.Time.now()
  goal.target_pose.pose.position.x = point[0]
  goal.target_pose.pose.position.y = point[1]
  goal.target_pose.pose.position.z = point[2]
  goal.target_pose.pose.orientation.w = 1.0

  CLIENT.send_goal(goal)

  while True:
    if check_image():
      CLIENT.cancel_all_goals()
      return True

    if CLIENT.wait_for_result(timeout = rospy.Duration(INTERVAL)):
      break

  if not CLIENT.wait_for_result():
    rospy.logerr("Action server not available")
    rospy.signal_shutdown("Action server not available")

  return False


def handle_inspect(req):
  global POINTS, INDEX

  while True:
    point = POINTS[INDEX]

    if send_goal(point):
      return TriggerResponse(
        success = True,
        message = "FOUND"
    )

    INDEX += 1
    INDEX %= len(POINTS or 1)


def handle_come_back(req):
  send_goal(POINTS[0]) # DISABLE TRASH RECOGNITION

  return TriggerResponse(
    success = True,
    message = "DONE"
  )


def handle_communication(req):
  return TriggerResponse(
    success = True,
    message = "DONE"
  )


def init_server():
  inspect_server = rospy.Service("/skills/inspect", Trigger, handle_inspect)
  come_back_server = rospy.Service("/skills/come_back", Trigger, handle_come_back)
  communication_server = rospy.Service("/skills/communication", Trigger, handle_communication)
  rospy.spin()


if __name__ == "__main__":
  rospy.init_node("navigation")

  with open(sys.argv[1]) as file:
    POINTS = yaml.safe_load(file)["points"]

  CLIENT = actionlib.SimpleActionClient("move_base", MoveBaseAction)
  CLIENT.wait_for_server()

  IMAGE = rospy.ServiceProxy("/obj_img_pos", ObjImgPos)
  IMAGE.wait_for_service()

  init_server()

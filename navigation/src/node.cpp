#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <nav_msgs/GetMap.h>
#include <geometry_msgs/Twist.h>
#include <std_srvs/Trigger.h>
#include <std_msgs/Float64MultiArray.h>
#include <string>
#include <stdexcept>
#include <vector>
#include <random>
#include <functional>

using namespace std;
using namespace ros;
using namespace nav_msgs;
using namespace std_srvs;
using namespace std_msgs;
using namespace move_base_msgs;
using namespace actionlib;
using namespace geometry_msgs;

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> Client;

class NavigationNode
{
private:
  Client client_;
  Publisher rotator_;
  Publisher coverage_;
  Publisher gripper_;
  ServiceServer service_;
  ServiceServer service2_;
  NodeHandle node_handle_;
  OccupancyGrid grid_;

  int RANGE = 3;
  const static int OBSTACLE_THRESHOLD = 50;
  const static int OBSTACLE_RANGE = 3 / 0.05;
  const static int ROTATE_SPEED = 30;
  const static int GRIPPER_SLEEP_DURATION = 2;
  const static int OBSTACLE = -1;
  const static int OBSTACLE_AREA = -100;
  const static int FREE = 0;
  const static int PROCESSED = 1;
  const static int SELECTED = 2;
  const static int VIEWBOX = 100;
  const pair<double, double> origin_ = { 0, 0 };

  vector<pair<int, int>> points_;
  int point_number_ = 0;

public:
  NavigationNode() : client_("move_base", true)
  {
    rotator_ = node_handle_.advertise<Twist>("cmd_vel", 1000);
    coverage_ = node_handle_.advertise<OccupancyGrid>("coverage_", 10, true);
    gripper_ = node_handle_.advertise<Float64MultiArray>("Gripper", 10);
  }

  inline void start()
  {
    while (!client_.waitForServer(Duration(5.0)))
    {
      cout << "Waiting for the move_base action server to come up\n";
    }

    getMap();
    prepareMap();
    getPoints();
    publishCoverage();

    point_number_ = 0;

    service_ = node_handle_.advertiseService("navigation/to_next_point", &NavigationNode::processNextPoint, this);
    service2_ = node_handle_.advertiseService("navigation/to_origin", &NavigationNode::returnToOrigin, this);
  }

private:
  struct Point
  {
    int row;
    int column;
    int distance_x;
    int distance_y;

    Point(int row, int column, int distance_x, int distance_y)
      : row{ row }, column{ column }, distance_x{ distance_x }, distance_y{ distance_y } {};
    Point(Point& previous, int dx, int dy)
      : row{ previous.row + dx }
      , column{ previous.column + dy }
      , distance_x{ previous.distance_x + dx }
      , distance_y{ previous.distance_y + dy } {};

    inline int distance()
    {
      return distance_x * distance_x + distance_y * distance_y;
    }
  };

  inline bool processNextPoint(Trigger::Request& req, Trigger::Response& res)
  {
    pair<double, double> real_world_point = toRealWorld(points_[point_number_]);

    res.message = sendRobot(real_world_point) ? "Success" : "Failure";
    res.success = true;

    point_number_++;
    if (point_number_ == points_.size())
    {
      point_number_ = 0;
    }

    return true;
  }

  inline bool returnToOrigin(Trigger::Request& req, Trigger::Response& res)
  {
    res.message = sendRobot(origin_) ? "Success" : "Failure";
    res.success = true;

    openAndCloseGripper();

    return true;
  }

  inline void publishCoverage()
  {
    int rows = grid_.info.height;
    int columns = grid_.info.width;

    for (pair<int, int> point : points_)
    {
      for (int x = max(0, point.first - RANGE); x < min(rows, point.first + RANGE + 1); x++)
      {
        if (point.second - RANGE >= 0)
        {
          gridSet(x, point.second - RANGE, VIEWBOX);
        }
        if (point.second + RANGE < columns)
        {
          gridSet(x, point.second + RANGE, VIEWBOX);
        }
      }
      for (int y = max(0, point.second - RANGE); y < min(columns, point.second + RANGE + 1); y++)
      {
        if (point.first - RANGE >= 0)
        {
          gridSet(point.first - RANGE, y, VIEWBOX);
        }
        if (point.first + RANGE < rows)
        {
          gridSet(point.first + RANGE, y, VIEWBOX);
        }
      }
    }

    coverage_.publish(grid_);
  }

  inline bool sendRobot(pair<double, double> point)
  {
    MoveBaseGoal goal;

    goal.target_pose.header.frame_id = "map";
    goal.target_pose.header.stamp = Time::now();

    goal.target_pose.pose.position.x = point.first;
    goal.target_pose.pose.position.y = point.second;
    goal.target_pose.pose.orientation.w = 1.0;

    cout << "Sending goal x: " << point.first << " y: " << point.second << '\n';

    client_.sendGoal(goal);
    client_.waitForResult();

    return client_.getState() == SimpleClientGoalState::SUCCEEDED;
  }

  inline void getMap()
  {
    GetMap::Request req;
    GetMap::Response res;

    service::call("/static_map", req, res);

    grid_ = res.map;

    RANGE = RANGE / res.map.info.resolution;
  }

  inline int gridGet(int row, int column)
  {
    if (row * grid_.info.width + column >= grid_.info.height * grid_.info.width || column >= grid_.info.width)
    {
      throw invalid_argument("Index out of bounds");
    }
    return grid_.data[row * grid_.info.width + column];
  }

  inline void gridSet(int row, int column, int value)
  {
    if (row * grid_.info.width + column >= grid_.info.height * grid_.info.width || column >= grid_.info.width)
    {
      throw invalid_argument("Index out of bounds");
    }
    grid_.data[row * grid_.info.width + column] = value;
  }

  inline void prepareMap()
  {
    int rows = grid_.info.height;
    int columns = grid_.info.width;

    for (int i = 0; i < rows; ++i)
    {
      for (int j = 0; j < columns; ++j)
      {
        if (gridGet(i, j) == OBSTACLE || gridGet(i, j) == OBSTACLE_AREA)
        {
          continue;
        }
        if (gridGet(i, j) >= OBSTACLE_THRESHOLD)
        {
          gridSet(i, j, OBSTACLE);
          for (int x = max(0, i - OBSTACLE_RANGE); x < min(rows, i + OBSTACLE_RANGE + 1); ++x)
          {
            for (int y = max(0, j - OBSTACLE_RANGE); y < min(columns, j + OBSTACLE_RANGE + 1); ++y)
            {
              if (gridGet(x, y) < OBSTACLE_THRESHOLD)
              {
                gridSet(x, y, OBSTACLE_AREA);
              }
              else
              {
                gridSet(x, y, OBSTACLE);
              }
            }
          }
        }
        else
        {
          gridSet(i, j, FREE);
        }
      }
    }
  }

  inline pair<double, double> toRealWorld(pair<int, int> point)
  {
    double resolution = grid_.info.resolution;
    double xOrigin = grid_.info.origin.position.x;
    double yOrigin = grid_.info.origin.position.y;

    return { resolution * point.first + xOrigin, resolution * point.second + yOrigin };
  }

  inline pair<int, int> getBounds(int firstParamMax, int secondParamMax, function<int(int, int)> getter)
  {
    pair<int, int> bounds = { 0, firstParamMax - 1 };

    for (; bounds.first <= bounds.second; ++bounds.first)
    {
      bool any = false;

      for (int i = 0; i < secondParamMax; ++i)
      {
        if (getter(bounds.first, i) == OBSTACLE)
        {
          any = true;
          break;
        }
      }

      if (any)
      {
        break;
      }
    }

    for (; bounds.first <= bounds.second; --bounds.second)
    {
      bool any = false;

      for (int i = 0; i < secondParamMax; ++i)
      {
        if (getter(bounds.second, i) == OBSTACLE)
        {
          any = true;
          break;
        }
      }

      if (any)
      {
        break;
      }
    }

    return bounds;
  }

  inline bool isPointWithinBounds(int row, int column, const pair<int, int>& row_bounds,
                                  const pair<int, int>& column_bounds)
  {
    return row >= row_bounds.first && row <= row_bounds.second && column >= column_bounds.first &&
           column <= column_bounds.second;
  }

  inline void traceRay(int centerX, int centerY, int pointX, int pointY)
  {
    double k = ((double)(centerY - pointY)) / (centerX - pointX);
    double b = centerY - k * centerX;

    function<double(int)> fun = [k, b](int x) { return k * x + b; };
    function<double(double, double)> difference = [](double a, double b) { return abs(a - b); };

    int x = centerX;
    int y = centerY;

    int dx = (centerX <= pointX) ? 1 : -1;
    int dy = (centerY <= pointY) ? 1 : -1;

    while (x != pointX && y != pointY)
    {
      if (gridGet(x, y) == OBSTACLE)
      {
        return;
      }
      if (gridGet(x, y) != OBSTACLE_AREA)
      {
        gridSet(x, y, PROCESSED);
      }
      if (difference(y, fun(x + dx)) < difference(y + dy, fun(x)))
      {
        x += dx;
      }
      else
      {
        y += dy;
      }
    }

    while (x != pointX)
    {
      if (gridGet(x, y) == OBSTACLE)
      {
        return;
      }
      if (gridGet(x, y) != OBSTACLE_AREA)
      {
        gridSet(x, y, PROCESSED);
      }
      x += dx;
    }

    while (y != pointY)
    {
      if (gridGet(x, y) == OBSTACLE)
      {
        return;
      }
      if (gridGet(x, y) != OBSTACLE_AREA)
      {
        gridSet(x, y, PROCESSED);
      }
      y += dy;
    }

    if (gridGet(x, y) != OBSTACLE && gridGet(x, y) != OBSTACLE_AREA)
    {
      gridSet(x, y, PROCESSED);
    }
  }

  inline void traceRays(int row, int column, const pair<int, int>& map_row_bounds,
                        const pair<int, int>& map_column_bounds)
  {
    for (int i = max(row - RANGE, map_row_bounds.first); i <= min(row + RANGE, map_row_bounds.second); ++i)
    {
      for (int j = max(column - RANGE, map_column_bounds.first); j <= min(column + RANGE, map_column_bounds.second);
           ++j)
      {
        traceRay(row, column, i, j);
      }
    }
  }

  inline pair<int, int> findNewPoint(int row, int column, const pair<int, int>& map_row_bounds,
                                     const pair<int, int>& map_column_bounds)
  {
    vector<Point> stack;

    gridSet(row, column, SELECTED);
    stack.push_back(Point(row, column, 0, 0));

    pair<int, int> row_bounds = { row, row };
    pair<int, int> column_bounds = { column, column };
    const vector<pair<int, int>> deltas = { { 1, 0 }, { -1, 0 }, { 0, 1 }, { 0, -1 } };

    while (!stack.empty())
    {
      Point p = stack.back();
      stack.pop_back();

      if (p.row < row_bounds.first)
      {
        row_bounds.first = p.row;
      }
      if (p.row > row_bounds.second)
      {
        row_bounds.second = p.row;
      }

      if (p.column < column_bounds.first)
      {
        column_bounds.first = p.column;
      }
      if (p.column > column_bounds.second)
      {
        column_bounds.second = p.column;
      }

      if (p.distance() > RANGE * RANGE)
      {
        continue;
      }

      for (auto delta : deltas)
      {
        if (isPointWithinBounds(p.row + delta.first, p.column + delta.second, map_row_bounds, map_column_bounds) &&
            gridGet(p.row + delta.first, p.column + delta.second) == FREE)
        {
          stack.push_back(Point(p, delta.first, delta.second));
          gridSet(p.row + delta.first, p.column + delta.second, SELECTED);
        }
      }
    }

    int mid_row = (row_bounds.first + row_bounds.second) >> 1;
    int mid_column = (column_bounds.first + column_bounds.second) >> 1;

    if (gridGet(mid_row, mid_column) != OBSTACLE && gridGet(mid_row, mid_column) != OBSTACLE_AREA)
    {
      return { mid_row, mid_column };
    }
    for (int delta = 0;; delta++)
    {
      if (mid_row - delta >= row_bounds.first && gridGet(mid_row - delta, mid_column) != OBSTACLE &&
          gridGet(mid_row - delta, mid_column) != OBSTACLE_AREA)
      {
        return { mid_row - delta, mid_column };
      }
      if (mid_row + delta <= row_bounds.second && gridGet(mid_row + delta, mid_column) != OBSTACLE &&
          gridGet(mid_row + delta, mid_column) != OBSTACLE_AREA)
      {
        return { mid_row + delta, mid_column };
      }
    }
  }

  inline void getPoints()
  {
    points_.clear();

    const pair<int, int> row_bounds =
        getBounds(grid_.info.height, grid_.info.height, [this](int row, int column) { return gridGet(row, column); });
    const pair<int, int> column_bounds =
        getBounds(grid_.info.width, grid_.info.height, [this](int column, int row) { return gridGet(column, row); });

    for (int i = row_bounds.first + RANGE; i <= row_bounds.second; i += 2 * RANGE)
    {
      for (int j = column_bounds.first + RANGE; j <= column_bounds.second; j += 2 * RANGE)
      {
        if (gridGet(i, j) != OBSTACLE && gridGet(i, j) != OBSTACLE_AREA)
        {
          traceRays(i, j, row_bounds, column_bounds);
          points_.push_back({ i, j });
        }
      }
    }

    for (int i = row_bounds.first; i <= row_bounds.second; ++i)
    {
      for (int j = column_bounds.first; j <= column_bounds.second; ++j)
      {
        while (gridGet(i, j) != OBSTACLE && gridGet(i, j) != OBSTACLE_AREA && gridGet(i, j) != PROCESSED)
        {
          pair<int, int> new_point = findNewPoint(i, j, row_bounds, column_bounds);
          traceRays(i, j, row_bounds, column_bounds);
          points_.push_back(new_point);
        }
      }
    }

    sort(points_.begin(), points_.end());
    for (int i = points_.size() - 2; i != 0; i--)
    {
      points_.push_back(points_[i]);
    }
  }

  inline void publishToGripper(double lift, double gripper)
  {
    Float64MultiArray msg;

    msg.data.resize(2);
    msg.data[0] = lift;
    msg.data[1] = gripper;

    gripper_.publish(msg);
  }

  inline void openAndCloseGripper()
  {
    publishToGripper(0, 1);
    Duration(GRIPPER_SLEEP_DURATION).sleep();
    publishToGripper(0, 0);
  }
};

int main(int argc, char** argv)
{
  init(argc, argv, "navigation");

  NavigationNode node;
  node.start();

  spin();
}

#include <ros.h>
#include <Servo.h>
#include <std_msgs/UInt16.h>
#include <std_srvs/SetBool.h>
#include <NewPing.h>

Servo servo_1, servo_2, servo_3;

const int up_angle=50;
const int down_angle=120;

const int servo_1_close = 7;
const int servo_2_close = 80;
const int servo_1_open = 87;
const int servo_2_open = 0;




NewPing front_sonar(10, 11, 300);
NewPing back_sonar(12, 13, 300);

class NewHardware : public ArduinoHardware
{
  public:
  NewHardware():ArduinoHardware(&Serial1, 115200){};
};
ros::NodeHandle_<NewHardware>  nh;

std_msgs::UInt16 front_dist, back_dist;

ros::Publisher chat_front("front_sonar", &front_dist);
ros::Publisher chat_back("back_sonar", &back_dist);



void callback(const std_srvs::SetBool::Request& request, std_srvs::SetBool::Response& response) {
  if (request.data == 1) {
    servo_3.write(down_angle);
    open_close( servo_1_close, servo_1_open, servo_2_close, servo_2_open);
    response.message = "OPEN";
  }
  else {
    open_close( servo_1_open, servo_1_close, servo_2_open, servo_2_close);
    servo_3.write(up_angle);
    response.message = "CLOSE";
  }
  response.success = true;
  
}

ros::ServiceServer<std_srvs::SetBool::Request, std_srvs::SetBool::Response> service("/dustpan", &callback);

void open_close(int servo_1_close, int servo_1_open, int servo_2_close, int servo_2_open) {
  int servo_1_step, servo_2_step, servo_1_dir, servo_2_dir;
  int servo_1_way = abs(servo_1_open - servo_1_close);
  int servo_2_way = abs(servo_2_open - servo_2_close);
  
  
  if (servo_1_close > servo_1_open) {
    servo_1_dir = -1;
  }
  else {
    servo_1_dir = 1;
  }
  if (servo_2_close > servo_2_open) {
    servo_2_dir = -1;
  }
  else {
    servo_2_dir = 1;
  }

  if (servo_1_way >= servo_2_way) {
    servo_2_step = 1;
    servo_1_step = servo_1_way / servo_2_way;
  }
  else {
    servo_1_step = 1;
    servo_2_step = servo_2_way / servo_1_way;
  }


  
  for (int i = servo_1_close, j = servo_2_close, k = 0; k < servo_1_way && k < servo_2_way; i += servo_1_dir * servo_1_step, j += servo_2_dir * servo_2_step, k++) {
    servo_1.write(i);
    servo_2.write(j);
    delay(20);
  }
  servo_1.write(servo_1_open);
  servo_2.write(servo_2_open);
  delay(500);
}



void setup() {
  pinMode(44, OUTPUT);
  pinMode(45, OUTPUT);
  pinMode(46, OUTPUT);
  
  pinMode(10, OUTPUT);
  pinMode(11, INPUT);
  pinMode(12, OUTPUT);
  pinMode(13, INPUT);

  pinMode(22, INPUT);
  
    
  nh.initNode();
  nh.advertiseService(service);
  nh.advertise(chat_front);
  nh.advertise(chat_back);

  servo_1.attach(44);
  servo_2.attach(45);
  servo_3.attach(46);

  servo_1.write(servo_1_close);
  servo_2.write(servo_2_close);
  

}

int last_button_condition = LOW;

void loop() {
  nh.spinOnce();

  if (digitalRead(22) != last_button_condition && last_button_condition == LOW) {
    servo_3.write(down_angle);
    open_close( servo_1_close, servo_1_open, servo_2_close, servo_2_open);
    last_button_condition = HIGH;
  }
  else if (digitalRead(22) != last_button_condition && last_button_condition == HIGH) {
    open_close( servo_1_open, servo_1_close, servo_2_open, servo_2_close);
    servo_3.write(up_angle);
    last_button_condition = LOW;
  }
  
  front_dist.data = front_sonar.convert_cm(front_sonar.ping_median(3));
  chat_front.publish(&front_dist);
  
  back_dist.data = back_sonar.convert_cm(back_sonar.ping_median(3));
  chat_back.publish(&back_dist);


}
